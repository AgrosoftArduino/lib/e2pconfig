#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <crc.h>

void loadEEPConfig(uint8_t *config, uint8_t *defConfig, void *startAddr,
                   uint16_t size) {
  eeprom_read_block(config, startAddr, size);

  if (crc16block(config, size) != 0) {
    uint8_t *buf = defConfig;
    uint8_t *c = config;
    for (uint16_t i = 0; i < size; i++) {
      *c = (uint8_t)pgm_read_byte(buf);
      buf++;
      c++;
    };
  }
}

void saveEEPConfig(uint8_t *config, void *startAddr, uint16_t size) {
  *((uint16_t *)(config + size - 2)) = crc16block(config, (size - 2));
  eeprom_write_block(config, startAddr, size);
}