#ifndef E2PCONFIG_H
#define E2PCONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void loadEEPConfig(uint8_t *config, uint8_t *defConfig, void *startAddr, uint16_t size);
void saveEEPConfig(uint8_t *config, void *startAddr, uint16_t size);

#ifdef __cplusplus
}
#endif

#endif
